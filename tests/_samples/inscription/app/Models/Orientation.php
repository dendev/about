<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orientation extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'proecohenallux';
    protected $table = 'orientations';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $appends = [];

    /*
   |--------------------------------------------------------------------------
   | FUNCTIONS
   |--------------------------------------------------------------------------
   */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function implantation()
    {
        return $this->hasOne(Implantation::class, 'src_id', 'key_impl');
    }

    public function study()
    {
        return $this->hasOne(Study::class, 'cursus_id', 'idCursus');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function  getLabelAttribute()
    {
        return $this->intitule;
    }

    public function  getLabelWithCodeAttribute()
    {
        return \Str::limit($this->intitule, 25) . ' - ' . $this->code;
    }

    public function  getCodeWithLabelAttribute()
    {
        return $this->code . ' - ' . $this->intitule;
    }

    public function  getCodeWithLabelAndImplantationAttribute()
    {
        return $this->code . ' - ' . $this->intitule . ' - ' . $this->implantation->code;
    }

    public function getImplantationLabelAttribute()
    {
        return $this->implantation->label;
    }

    public function  getLabelFullAttribute()
    {
        return $this->label . ' à ' . $this->implantation_label;
    }

    public function  getLabelWithSectionAttribute()
    {
        $full_label = $this->label;

        if( $this->sousect != '-' )
            $full_label  .= ', ' . lcfirst($this->sousect);

        return $full_label;
    }

    public function  getLabelWithSectionAndCodeAttribute()
    {
        $full_label = $this->label;

        if( $this->sousect != '-' )
            $full_label .= ', ' . lcfirst($this->sousect);

        if( $this->code )
            $full_label .= ' ( ' . $this->code . ' )';

        return $full_label;
    }

    public function  getLabelWithSectionAndCodeAndImplantationAttribute()
    {
        $full_label = $this->label;

        if( $this->sousect != '-' )
            $full_label .= ', ' . lcfirst($this->sousect);

        if( $this->code )
            $full_label .= ' ( ' . $this->code . ' )';

        if( $this->implantation )
            $full_label .= ' ( ' . $this->implantation->code . ' )';

        return $full_label;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
