<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ZeroDaHero\LaravelWorkflow\Traits\WorkflowTrait;

class Document extends Model
{
    use CrudTrait;
    use HasFactory;
    use WorkflowTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'documents';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
     protected $dates = [
         'claimed_at',
         'provided_at',
         'validated_at',
         'invalidated_at',
     ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function requester()
    {
        return $this->hasOne(User::class, 'id', 'claimed_by');
    }

    public function library()
    {
        return $this->hasOne(Library::class, 'id', 'library_id');
    }

    public function demand()
    {
        return $this->hasOne(Demand::class, 'id', 'demand_id');
    }

    public function status()
    {
        return $this->hasOne(DocumentStatus::class, 'id', 'status_id');
    }

    public function historic()
    {
        return $this->hasMany(DocumentStatus::class, 'document_id', 'id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getUrlAttribute()
    {
        return Document . phpenv('APP_URL');
    }

    public function getFullPathAttribute()
    {
        return Storage::path($this->path);
    }

    public function getExtensionAttribute()
    {
        return pathinfo($this->full_path, PATHINFO_EXTENSION);
    }

    public function getFilenameAttribute()
    {
        // basic
        $demand = Demand::with('user')->find($this->demand_id);
        $user = $demand->user;

        // file without proeco
        $filename = $this->label . '_' . $demand->id . '_' . $user->name;

        // file proeco
        if( ! is_null($user->numproeco) )
            $filename .= '_' . $user->numproeco;

        // slugify
        $filename = \Str::slug($filename);

        // end
        return $filename;
    }

    public function getIsImageAttribute()
    {
        $is_image = false;

        $images_ext = ['png', 'jpg', 'jpeg'];
        if( ! is_null( $this->path ) )
        {
            $info = pathinfo($this->path);
            if( in_array($info['extension'], $images_ext) )
            {
                $is_image = true;
            }
        }

        return $is_image;
    }

    public function getDebugAttribute()
    {
        $debug = [
            'id' => $this->id,
            'markings' => $this->marking
        ];

        return $debug;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
