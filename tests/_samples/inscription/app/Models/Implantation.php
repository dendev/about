<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Implantation extends Model
{
    use CrudTrait;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $connection = 'mysql';
    protected $table = 'implantations';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $appends = [];

    /*
   |--------------------------------------------------------------------------
   | FUNCTIONS
   |--------------------------------------------------------------------------
   */
    public function get_relais_esi()
    {
        $relais = false;
        $academic_year = \AcademicYearManager::current('sheldon');
      //  $relais = DB::connection('sheldon')->select('select distinct id_people, nom, prenom, email from penny.relcopilot(?) WHERE numero = ?',[2021, 12]);
// error        DETAIL:  Final statement returns smallint instead of integer at column 9.

        //dd( $relais);
        return $relais;

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function orientations()
    {
        return $this->hasMany('App\Models\Orientation', 'key_impl', 'src_id');
    }

    public function director()
    {
        return $this->hasOne(User::class, 'id', 'director_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function  getLabelWithCodeAttribute()
    {
        return $this->label . ' ( ' . $this->code . ')';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
