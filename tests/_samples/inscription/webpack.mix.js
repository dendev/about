const mix = require('laravel-mix');
const cssNesting = require('postcss-nesting');

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/mdb_init.js', 'public/js')
    .js('resources/js/crop.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .postCss('resources/css/app.css', 'public/css/tall-form.css', [
        require('postcss-import'),
        cssNesting(),
        require('tailwindcss'),
        // require('autoprefixer'), automatic w Laravel Mix 6
    ])

if (mix.inProduction()) {
    mix.version();
}
