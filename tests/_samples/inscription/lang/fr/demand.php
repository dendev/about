<?php

/**
 * Concerne les textes affichés sur la demande ou la liste des demandes
 */

return [
    "title_candidate" => "Ma demande",
    "title_secretary" => "Demande",
    "subtitle_secretary" => "Informations au sujet de la demande numéro ",
    'list_title' => 'Demandes',
    'list_subtitle' => 'Gestion des demandes : ',
    "info_holiday" => "Merci d'avoir entamé ta démarche d'inscription à l'Henallux. Reste bien attentif à ta boîte mails.",
    "error_uncreated_title" => "Erreur dans la création de la demande",
    "error_uncreated_description" => "Une erreur est survenue, merci de réessayer.",
    'help' => "Vous pouvez afficher toutes les demandes ou celles réclamant une action de votre part.
<br> Pour agir sur une demande, il faut cliquer sur le 2e bouton. Celui-ci affiche la liste des actions possibles sur cette demande.
<br><br> Une action grisée avec une icône de calendrier indique que vous devez attendre que le candidat agisse.
<br> Les actions avec un V vert indiquent qu'elles sont terminées." ,
    "modal_help_label" => "Aide",
    "modal_help_close" => "Fermer",
    'user_is_sf_nb_hint' => 'Etudiant(e) SF non Belge.',
    'user_no_profile_hint' => 'Candidat sans profil',
    'no_datas_infos' => 'Aucune donnée',

    // summary
    'summary_label' => 'Récapitulatif',
    'summary_at' => 'Demande faite le',

    // candidate
    //  //  actions
    "action_infos" => "Nous attirons ton attention sur le fait que, durant les congés scolaires, le traitement du dossier est ralenti.",
    //  //  //  form
    "action_form_label" => "Introduire ma demande d'inscription",
    "action_form_desc" => "Remplis et envoie-nous le formulaire ci-dessous.",
    "action_form_btn_label" => "Formulaire",
    //  //  //  form edit
    "action_form_edit_label" => "Corriger le formulaire",
    "action_form_edit_desc" => "Tu dois corriger le formulaire et l'envoyer pour vérification.",
    "action_form_edit_btn_label" => "Formulaire",
    //  //  //  document
    "action_doc_upload_label" => "Gestion des documents",
    "action_doc_upload_desc" => "",
    "action_doc_upload_btn_label" => "Envoie nous tes documents",
    //  //  //  cancel
    "action_cancel_label" => "Annuler",
    "action_cancel_desc" => "Tu souhaites annuler cette demande de préinscription.",
    "action_cancel_btn_label" => "Annuler",
    //  //  //  redirect
    "action_redirect_label" => "Contact",
    "action_redirect_desc" => "Il faut prendre contact avec le secrétariat.",
    "action_redirect_btn_label" => "Secrétariat",
    // secretary
    //  //  filters
    'filters_all_label' => 'Toutes',
    'filters_all_hint' => 'Voir toutes les demandes',
    'filters_form_waiting_label' => 'Form',
    'filters_form_waiting_hint' => 'Il faut vérifier le formulaire',
    'filters_encode_waiting_label' => 'Encodage',
    'filters_encode_waiting_hint' => 'Demande à encoder',
    'filters_docs_unasked_label' => 'D. à réclamer',
    'filters_docs_unasked_hint' => 'Il faut réclamer les documents',
    'filters_docs_waiting_label' => 'D. à vérifier',
    'filters_docs_waiting_hint' => 'Il faut vérifier les documents',
    'filters_docs_pending_label' => 'D. à finaliser',
    'filters_docs_pending_hint' => "Il faut terminer l'étape document ou en demander de nouveaux si besoin",
    'filters_end_pending_label' => 'Terminé',
    'filters_end_hint' => "Demandes finalisées",
    //  //  actions
    //  //  //  help
    'help_label' => 'Aide',
    'help_status_label' => 'Statut',
    'help_instructions_label' => 'Instructions',
    'help_instruction_secretary_label' => 'Secrétariat',
    'help_instruction_candidate_label' => 'Candidat',
    'help_historic_label' => 'Historique',
    'help_historic_at_label' => 'Date',
    'help_historic_status_label' => 'Statut',
    'help_btn_close_label' => 'Fermer',
    //  //   // info
    'action_info_label' => "Afficher l'aide",
    //  //  //  actions
    'action_actions_label' => "Afficher les actions possibles",
    //  //  //  show
    'action_show_label' => 'Afficher la demande',
    //  //  //  user
    'action_user_label' => 'Afficher les informations du candidat',
    //  //  //  contextual action
    'action_item_done_hint' => 'Finalisé',
    'action_item_current_hint' => "Disponible",
    'action_item_future_hint' => "À venir",
    'action_item_info_label' => 'Infos',
    'action_item_form_label' => 'Formulaire',
    'action_item_encode_label' => 'Encodage',
    'action_item_document_label' => 'Document',
    'action_item_payment_label' => 'Paiement',
    'action_item_mail_label' => 'Mail', // not really contextual action
    'action_item_notes_label' => 'Notes', // must be levelup
    'action_item_diagnostic_label' => 'Diagnostic',
    'action_item_edit_label' => "Modifier",
    'action_item_delete_label' => "Supprimer",

    // itinerary
    'itinerary_label' => "",
    'itinerary_btn_show' => 'Afficher',
    'itinerary_form_label' => "Formulaire",
    'itinerary_form_description' => "Envoi du formulaire complété au secrétariat.",
    'itinerary_encode_label' => "Encodage",
    'itinerary_encode_description' => "Introduction des données dans le système informatique.",
    'itinerary_document_label' => "Documents",
    'itinerary_document_description' => "Envoi des documents au secrétariat.",
    'itinerary_payment_label' => "Paiement",
    'itinerary_payment_description' => "Paiement de l'acompte.",

    'itinerary_state_success_label' => "fini",
    'itinerary_state_error_label' => "à corriger",
    'itinerary_state_waiting_label' => "en attente",
    'itinerary_state_current_label' => "en cours",
    'itinerary_state_future_label' => "à venir",

    // note
    'note_title' => 'Notes',
    'note_subtitle' => 'Ajout de notes personnelles à la demande',
    'note_help' => "Vous pouvez ajouter des notes utiles au traitement de la demande.<br>Ces notes ne seront pas vues par le candidat.",
    'note_add_label' => 'Ajout',
    'note_add_content_label' => 'Contenu de la note',
    'note_submit_label' => 'Valider',
    'note_error' => 'Erreur lors de la sauvegarde',
    'note_not_datas' => 'Aucune note disponible',

    // mail
    'mail_title' => 'Mails',
    'mail_subtitle' => 'Historique des mails',
    'mail_listing_label' =>  "Mails envoyés",
    'mail_id_label' =>  "#",
    'mail_at_label' =>  "Le",
    'mail_subject_label' =>  "Sujet",
    'mail_to_label' =>  "Destinataire",
    'mail_actions_label' =>  "Actions",
    'mail_btn_show_tooltip' => 'Voir',
    'mail_btn_send_tooltip' => 'Réenvoyer',
    //  //  show
    'mail_show_label' => 'Détails',
    'mail_limit_label' => 'Limite',
    'mail_limit_infinite' => 'Illimité',
    'mail_content_label' => 'Contenu',
    //  //  send
    'mail_send_success' => 'Mail envoyé',
    'mail_send_warning' => "La limite d'envoi ne permet pas d’envoyer à nouveau ce mail",
    'mail_send_error' => "Erreur lors de l'envoi du mail",

    // edit
    'edit_title' => 'Modification',
    'edit_subtitle' => "Modification de certaines données de la demande",
    'edit_help' => "L'email est l'élément unique et principal de contact. Il doit théoriquement etre personel au candidat.<br>
Le changement d'orientation impacte le traitement de la demande de multiples façon.<br><br>
Une demande encodé et modifié depuis inscription ne serat plus cohérente.<br>
Une demande dont les documents sont acceptés qui change ensuite d'orientation et donc de contraites de documents peut permettre invalide d'être accepter.<br><br>
Les conséquences d'un changement sur la demande doivent être réfléchies et ne donneront pas lieu à un support informatique.",
    'edit_warning_msg' => "Les modifications faites sur la demande sont à la responsabilité du secrétariat",
    'edit_email_label' => 'Email',
    'edit_orientation_label' => 'Orientation',
    'edit_submit_label' => 'Modifier',
    'edit_success_msg' => 'Modification(s) effectuée(s)',

    // delete
    'delete_title' => 'Supprimer',
    'delete_subtitle' => "Supprime la demande",
    'delete_help' => "Une demande supprimé ne peut pas être récupérer<br>
La suppression d'une demande peut entrainer des erreurs ou traitement supplémentaires à charge de l'utilisation.<br>
Comme la gestion du remboursement ou non de l'acompte, la cohérence avec proeco, ....<br>
Inscription prend en charge la suppression de la demande dans sa base de données et s'arrète à ça.
",
    'delete_warning_msg' => "La suppression est définitive et ses conséquences sont à charge de l'utilisateur",
    'delete_submit_label' => 'Supprimer',
    'delete_success_msg' => 'Suppréssion effectuée',

    // create by secretary
    'create_by_secretary_label' => 'Création de demandes',
    'create_by_secretary_hint' => 'Création de demande par le secrétariat ( HUE, Codiplomation, FreeMover, ... )',
    'create_by_secretary_title' => 'Création de demandes',
    'create_by_secretary_subtitle' => "Création à l'initiative du secrétariat",
    'create_by_secretary_help' => "",
    'create_by_secretary_out_of_time_late_menu_label' => 'Inscription tardive',
    'create_by_secretary_out_of_time_reorientation_menu_label' => 'Réorientation',
    'create_by_secretary_out_of_time_modify_menu_label' => 'Modification',
    'create_by_secretary_codiplomation_menu_label' => 'Codiplomation',
    'create_by_secretary_erasmus_menu_label' => 'Erasmus',
    'create_by_secretary_free_mover_menu_label' => 'FreeMover',
    'create_by_secretary_free_student_menu_label' => 'Elève libre',
    'create_by_secretary_hue_menu_label' => 'HUE',
    'create_by_secretary_success_label' => "Demande crée",
    'create_by_secretary_success_msg' => "La demande à bien été créée et porte le numéro :demand_id",
    'create_by_secretary_error_label' => "Erreur à la création",
    'create_by_secretary_error_msg' => "La demande n'a pas pu être créée",
    //  //  general form
    'create_by_secretary_email_label' => "Email du candidat",
    'create_by_secretary_firstname_label' => "Prénom du candidat",
    'create_by_secretary_lastname_label' => "Nom du candidat",
    'create_by_secretary_sexe_label' => "Genre",
    'create_by_secretary_sexe_select_label' => "Choix du genre",
    'create_by_secretary_birthday_label' => "Date de naissance",
    'create_by_secretary_orientation_label' => "Orientation",
    'create_by_secretary_orientation_select_label' => "Choix de l'orientation",
    'create_by_secretary_profile_label' => "Profil du candidat",
    'create_by_secretary_profile_hint' => "Le profil du candidat détermine le formulaire qu'il devra compléter",
    'create_by_secretary_submit_label' => "Créer la demande",
    //  //  //  hue
    'create_by_secretary_hue_label' => "Création d'une demande HUE",
    'create_by_secretary_hue_description' => "Demande classique mais pour un candidat non européen",
        //  //  // out of time late
    'create_by_secretary_out_of_time_late_label' => "Création d'une inscription tardive",
    'create_by_secretary_out_of_time_late_description' => "Etudiant s'inscrivant tardivement du 1 octobre au 15 février.",
    //  //  // out of time reorientation
    'create_by_secretary_out_of_time_reorientation_label' => "Création d'une réorientation",
    'create_by_secretary_out_of_time_reorientation_description' => "Etudiant de première année du premier cycle modifiant son inscription du 1 novembre au 15 février.",
    //  //  // out of time modify
    'create_by_secretary_out_of_time_modify_label' => "Modification d'inscription",
    'create_by_secretary_out_of_time_modify_description' => "Etudiant de première année du premier cycle modifiant sont inscription en venant d'un autre établissement du 1 octobre au 31 octobre",
    //  //  //  codiplomation
    'create_by_secretary_codiplomation_label' => "Encodage d'une codiplomation",
    'create_by_secretary_codiplomation_description' => "Le candidat est créé dans proeco avec les informations minimales fournies par le secrétariat.
    <br>Il ne remplit pas le formulaire, n’a pas d’invitation à payer son acompte de 50 euros et ne fournit aucun document.",
    //  //  //  free_mover
    'create_by_secretary_free_mover_label' => "Encodage d'un freemover",
    'create_by_secretary_free_mover_description' => "Le candidat est créé dans proeco avec les informations minimales fournies par le secrétariat.
    <br>Il ne remplit pas le formulaire, n’a pas d’invitation à payer un acompte et ne fournit aucun document",
    //  //  //  free_student
    'create_by_secretary_free_student_label' => "Encodage d'un étudiant libre",
    'create_by_secretary_free_student_description' => "Création d'une demande pour un étudiant libre",
    'create_by_secretary_free_student_info' => "Procédure non définie",
    //  //  //  erasmsus
    'create_by_secretary_erasmus_label' => "Etudiant Erasmus",
    'create_by_secretary_erasmus_description' => "Infos au sujet des Erasmus",
    'create_by_secretary_erasmus_info' => "La gestion des Erasmus se fait via la plateforme <a href='https://services.henallux.be/international/' target='_blank'>international</a>",
];

