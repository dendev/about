<?php

return [
    'id' => '#',
    'title' => 'Bibliothèque',
    'list_label' => 'Liste',
    'item_label' => 'Document',
    'status_label' => 'Statut',
    'label' => 'Label',
    'identity' => 'Code',
    'category' => 'Catégorie',
    'priority' => 'Mit en avant dans la liste',
    'description' => 'Description',
    'is_for_mydocs' => 'Est destiné à Mydocs',
    'is_for_supervisor' => 'Est destiné à Digitalisation',
    'allowed_types' => 'Type(s) autorisé(s)',
    'allowed_pdf' => 'Autorisé le pdf ( .pdf )',
    'allowed_image' => 'Autorisé les images ( .jpeg, .jpg, .png )',
    'status' => 'Statut',
    'creation_requested_by' => "Création demandé par le secrétariat",
    'creation_requested_at' => "Date demande de création secrétariat",
    'creation_accepted_by' => "Création accepté par la direction",
    'creation_accepted_at' => "Date demande d'acceptation direction",
    'creation_approved_by' => "Création approuvé par AA",
    'creation_approved_at' => "Date demande approuvé AA",
    'created_at' => "Crée le",

    // filters
    'filter_category_label' => 'Catégorie',
    'filter_is_for_mydocs_label' => 'Mydocs',
    'filter_is_for_supervisor_label' => 'Digit.',
    'filter_waiting_aa_label' => 'A vérifier',

    // status
    'status_id' => "#",
    'library_label' => "Document",
    'type_label' => "Statut",

    // actions
    'accepted_btn_label' => 'Accepter',
    'refused_btn_label' => 'Refuser',
    'accepted_success' => "Document approuvé",
    'accepted_error' => "Erreure lors de l'acceptation",
    'refused_success' => "Doument refusé",
    'refused_error' => "Erreure lors du refus",
    'accepted_network_error' => "Erreur lors du traitement",
    'refused_network_error' => "Erreur lors du traitement",
];
