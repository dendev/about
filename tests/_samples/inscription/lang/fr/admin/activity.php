<?php

return [
    'id' => '#',
    'causer' => 'Utilisateur',
    'description' => 'Description',
    'created_at' => 'Date',

    //filters
    'filter_candidate_label' => 'Candidat',
    'filter_secretary_label' => 'Secrétariat',
    'filter_dates_label' => 'Dates',
];

