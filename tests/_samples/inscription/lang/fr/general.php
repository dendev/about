<?php

/**
 * Phrases qu'on va retrouver n'importe où dans le site
 */

return [
    'menu_auth_login' => 'Connexion',
    'menu_auth_logout' => 'Déconnexion',
    'menu_home' => 'Accueil',
];
