<?php
namespace Tests\Parsers;

use Dendev\About\Parsers\EnvParser;
use PHPUnit\Framework\TestCase;


final class EnvParserTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/.env';

        $env_parser = new EnvParser($full_path);
        $values = $env_parser->resolve();

        $this->assertIsArray($values);
        // check have basic datas
        $this->assertArrayHasKey('log_channel', $values);
        $this->assertArrayHasKey('app_debug', $values);
        $this->assertArrayHasKey('mail_mailer', $values);
        // check removed password
        $this->assertArrayNotHasKey('app_key', $values);
        $this->assertArrayNotHasKey('henallux_client_id', $values);
        $this->assertArrayNotHasKey('db_password', $values);
    }
}
