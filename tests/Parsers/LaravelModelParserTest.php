<?php
namespace Tests\Parsers;

use Dendev\About\Parsers\LaravelModelParser;
use PHPUnit\Framework\TestCase;


final class LaravelModelParserTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/app/Models/Implantation.php';

        $laravel_model_parser = new LaravelModelParser($full_path);
        $values = $laravel_model_parser->resolve();

        // check have basic datas
        $this->assertArrayHasKey('db_identity', $values);
        $this->assertArrayHasKey('db_type', $values);
        $this->assertArrayHasKey('server_identity', $values);
        $this->assertArrayHasKey('server_ip', $values);
        $this->assertArrayHasKey('table_identity', $values);
        $this->assertArrayHasKey('table_name', $values);
        $this->assertEquals('inscription2_1', $values['db_identity']);
        $this->assertEquals('mariadb', $values['db_type']);
        $this->assertEquals('db_mariadb_1', $values['server_identity']);
        $this->assertEquals('127.0.0.1', $values['server_ip']);
        $this->assertEquals('inscription2_1-implantations', $values['table_identity']);
        $this->assertEquals('implantations', $values['table_name']);
    }

    public function testResolveWithDefaultConnection()
    {
        $full_path = __DIR__ . '/../_samples/inscription/app/Models/MyNote.php';

        $getter = new LaravelModelParser($full_path);
        $values = $getter->resolve();

        // check have basic datas
        $this->assertArrayHasKey('db_identity', $values);
        $this->assertArrayHasKey('db_type', $values);
        $this->assertArrayHasKey('server_identity', $values);
        $this->assertArrayHasKey('server_ip', $values);
        $this->assertArrayHasKey('table_identity', $values);
        $this->assertArrayHasKey('table_name', $values);
        $this->assertEquals('inscription2_1', $values['db_identity']);
        $this->assertEquals('mariadb', $values['db_type']);
        $this->assertEquals('db_mariadb_1', $values['server_identity']);
        $this->assertEquals('127.0.0.1', $values['server_ip']);
        $this->assertEquals('inscription2_1-my_notes', $values['table_identity']);
        $this->assertEquals('my_notes', $values['table_name']);
    }
}
