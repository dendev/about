<?php
namespace Tests\Parsers;

use Dendev\About\Parsers\ComposerParser;
use Dendev\About\Parsers\EnvParser;
use PHPUnit\Framework\TestCase;


final class PackageParserTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/package.json';

        $env_parser = new ComposerParser($full_path);
        $values = $env_parser->resolve();

        $this->assertIsArray($values);
        // check have basic datas
        $this->assertArrayHasKey('dependencies', $values);
        $this->assertArrayHasKey('devDependencies', $values);
        $this->assertArrayHasKey('scripts', $values);
    }
}
