<?php
namespace Tests\Getters;

use Dendev\About\Getters\GitGetter;
use PHPUnit\Framework\TestCase;


final class GitGetterTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../../';

        $getter = new GitGetter($full_path);
        $values = $getter->resolve();

        $this->assertIsArray($values);
        // check have basic datas
        $this->assertArrayHasKey('use_git', $values);
        $this->assertArrayHasKey('current_branch', $values);
        $this->assertArrayHasKey('current_commit', $values);
        $this->assertArrayHasKey('remote_repository', $values);
    }
}
