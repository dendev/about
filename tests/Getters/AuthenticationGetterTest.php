<?php
namespace Tests\Getters;

use Dendev\About\Getters\AuthenticationGetter;
use Dendev\About\Getters\GeneralGetter;
use PHPUnit\Framework\TestCase;


final class AuthenticationGetterTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/';

        $getter = new AuthenticationGetter($full_path);
        $values = $getter->resolve();

        $this->assertIsArray($values);
        // check have basic datas
        $this->assertArrayHasKey('use_cas', $values);
        $this->assertArrayHasKey('use_saml', $values);

        $this->assertFalse( $values['use_cas']);
        $this->assertTrue( $values['use_saml']);
    }
}
