<?php
namespace Tests\Getters;

use Dendev\About\Getters\GeneralGetter;
use PHPUnit\Framework\TestCase;


final class GeneralGetterTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/';

        $getter = new GeneralGetter($full_path);
        $values = $getter->resolve();

        $this->assertIsArray($values);
        // check have basic datas
        $this->assertArrayHasKey('app_name', $values);
        $this->assertArrayHasKey('app_type', $values);
        $this->assertArrayHasKey('php_required', $values);

        $this->assertEquals('Inscription', $values['app_name']);
        $this->assertEquals('laravel', $values['app_type']);
        $this->assertEquals('^7.3|^8.0', $values['php_required']);

        // check have specific datas
        $this->assertEquals($values['app_name'], $getter->get('env.app_name'));
    }
}
