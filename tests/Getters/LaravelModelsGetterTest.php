<?php
namespace Tests\Getters;

use Dendev\About\Getters\LaravelModelsGetter;
use PHPUnit\Framework\TestCase;


final class LaravelModelsGetterTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/app/Models/';

        $getter = new LaravelModelsGetter($full_path);
        $values = $getter->resolve();

        // check have basic datas
        $this->assertArrayHasKey('activity_log', $values);
        $this->assertArrayHasKey('cursus', $values);
        $this->assertArrayHasKey('documents', $values);
        $this->assertIsArray($values['activity_log']);
    }
}
