<?php
namespace Tests\Getters;

use Dendev\About\Getters\GitGetter;
use Dendev\About\Getters\LaravelModelGetter;
use PHPUnit\Framework\TestCase;


final class LaravelModelGetterTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/app/Models/Implantation.php';

        $getter = new LaravelModelGetter($full_path);
        $values = $getter->resolve();

        // check have basic datas
        $this->assertArrayHasKey('db_identity', $values);
        $this->assertArrayHasKey('db_type', $values);
        $this->assertArrayHasKey('server_identity', $values);
        $this->assertArrayHasKey('server_ip', $values);
        $this->assertArrayHasKey('table_identity', $values);
        $this->assertArrayHasKey('table_name', $values);
        $this->assertEquals('inscription2_1', $values['db_identity']);
        $this->assertEquals('mariadb', $values['db_type']);
        $this->assertEquals('db_mariadb_1', $values['server_identity']);
        $this->assertEquals('127.0.0.1', $values['server_ip']);
        $this->assertEquals('inscription2_1-implantations', $values['table_identity']);
        $this->assertEquals('implantations', $values['table_name']);
    }

    public function testResolveWithDefaultConnection()
    {
        $full_path = __DIR__ . '/../_samples/inscription/app/Models/MyNote.php';

        $getter = new LaravelModelGetter($full_path);
        $values = $getter->resolve();

        // check have basic datas
        $this->assertArrayHasKey('table_name', $values);
        $this->assertArrayHasKey('table_identity', $values);
        $this->assertArrayHasKey('server_identity', $values);
        $this->assertArrayHasKey('server_ip', $values);
        $this->assertArrayHasKey('server_username', $values);
        $this->assertEquals('my_notes', $values['table_name']);
        $this->assertEquals('inscription2_1-my_notes', $values['table_identity']);
        $this->assertEquals('mariadb', $values['db_type']);
        $this->assertEquals('db_mariadb_1', $values['server_identity']);
        $this->assertEquals('127.0.0.1', $values['server_ip']);
    }
}
