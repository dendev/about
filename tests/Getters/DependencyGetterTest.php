<?php
namespace Tests\Getters;

use Dendev\About\Getters\DependencyGetter;
use PHPUnit\Framework\TestCase;


final class DependencyGetterTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../../';

        $getter = new DependencyGetter($full_path);
        $values = $getter->resolve();

        $this->assertIsArray($values);
        // check have basic datas
        $this->assertArrayHasKey('use_laravel', $values);
        $this->assertArrayHasKey('laravel_version', $values);
        $this->assertArrayHasKey('use_backpack', $values);
        $this->assertArrayHasKey('backpack_version', $values);
        $this->assertArrayHasKey('use_filament', $values);
        $this->assertArrayHasKey('filament_version', $values);
    }
}
