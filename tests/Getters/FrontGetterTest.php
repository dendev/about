<?php
namespace Tests\Getters;

use Dendev\About\Getters\FrontGetter;
use PHPUnit\Framework\TestCase;


final class FrontGetterTest extends TestCase
{
    private static array $_config;
    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');
        self::$_config = $config;
    }

    public function testResolve()
    {
        $full_path = __DIR__ . '/../_samples/inscription/';

        $getter = new FrontGetter($full_path);
        $values = $getter->resolve();

        $this->assertIsArray($values);
        // check have basic datas
        $this->assertArrayHasKey('use_livewire', $values);
        $this->assertArrayHasKey('use_codyhouse', $values);
        $this->assertArrayHasKey('use_bootstrap', $values);
        $this->assertArrayHasKey('use_vite', $values);
        $this->assertArrayHasKey('use_webpack', $values);
        $this->assertArrayHasKey('use_sass', $values);
        $this->assertArrayHasKey('use_postcss', $values);
    }
}
