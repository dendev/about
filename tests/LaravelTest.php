<?php

use Dendev\About\Laravel\Manager;
use PHPUnit\Framework\TestCase;


final class LaravelTest extends TestCase
{
    private static $_manager;
    private static $_config;

    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');

        self::$_config = $config;
        self::$_manager = new Manager();
    }

    public function testFindEnv()
    {
        $project_name = self::$_config['projects_name'][0];
        $project_env_path = self::$_config['projects_env_path'][0];

        $data = self::$_manager->find_env();
        $this->assertNotEmpty($data);
        $this->assertArrayHasKey($project_name, $data);;
        $this->assertEquals($project_env_path, $data[$project_name]);
    }

    public function testGetPathByProjectName()
    {
        $project_name = self::$_config['projects_name'][0];
        $project_path = self::$_config['projects_path'][0];

        $data = self::$_manager->get_path_by_project_name($project_name);
        $this->assertEquals($project_path, $data);
    }

    public function testReadEnv()
    {
        $project_name = self::$_config['projects_name'][0];
        $project_env_path = self::$_config['projects_env_path'][0];
        $project_env_content = self::$_config['projects_env_content'][$project_name];
        $keys = array_keys($project_env_content);

        $data = self::$_manager->read_env($project_env_path);
        $this->assertArrayHasKey($keys[0], $data);
        $this->assertEquals($data[$keys[0]], $project_env_content[$keys[0]]);
    }

    public function testCheckUseProecohenallux()
    {
        $project_env_path = self::$_config['projects_env_path'][0];

         $data = self::$_manager->check_use_proecohenallux($project_env_path);
        $this->assertTrue($data);
    }

    public function testGetProecohenalluxValues()
    {
        $project_env_path = self::$_config['projects_env_path'][0];

        $data = self::$_manager->get_proecohenallux_values($project_env_path);
        $this->assertNotEmpty($data);
        $this->assertArrayHasKey('db_proecohenallux_connection', $data);
        $this->assertFalse( $data['is_general']);
    }

    public function testCheckDebugOff()
    {
        $project_env_path = self::$_config['projects_env_path'][0];

        $is_debug = self::$_manager->check_is_debug($project_env_path);
        $this->assertTrue($is_debug);
    }
}
