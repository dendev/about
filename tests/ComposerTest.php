<?php

use Dendev\About\Composer\Manager;
use PHPUnit\Framework\TestCase;


final class ComposerTest extends TestCase
{
    private static $_manager;
    private static $_config;

    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');

        self::$_config = $config;
        self::$_manager = new Manager();
    }

    public function testFindComposer()
    {
        $project_name = self::$_config['projects_name'][0];
        $project_env_path = self::$_config['projects_composer_path'][0];

        $data = self::$_manager->find_composer();
        $this->assertNotEmpty($data);
        $this->assertArrayHasKey($project_name, $data);;
        $this->assertEquals($project_env_path, $data[$project_name]);
    }

    public function testReadComposer()
    {
        $project_composer_path = self::$_config['projects_composer_path'][0];

        $data = self::$_manager->read_composer($project_composer_path);
        $this->assertNotEmpty($data);
    }

    public function testGetPhpInfo()
    {
        $project_name = self::$_config['projects_name'][0];
        $project_composer_path = self::$_config['projects_composer_path'][0];
        $project_composer_content = self::$_config['projects_composer_content'][$project_name];

        $data = self::$_manager->get_php_info($project_composer_path);
        $this->assertIsString($data);
        $this->assertEquals($data, $project_composer_content['php']);
    }

    public function testGetBackpackInfo()
    {
        $project_name = self::$_config['projects_name'][0];
        $project_composer_path = self::$_config['projects_composer_path'][0];
        $project_composer_content = self::$_config['projects_composer_content'][$project_name];

        $data = self::$_manager->get_backpack_info($project_composer_path);
        $this->assertIsString($data);
        $this->assertEquals($data, $project_composer_content['backpack/crud']);
    }

    public function testGetLaravelInfo()
    {
        $project_name = self::$_config['projects_name'][0];
        $project_composer_path = self::$_config['projects_composer_path'][0];
        $project_composer_content = self::$_config['projects_composer_content'][$project_name];

        $data = self::$_manager->get_laravel_info($project_composer_path);
        $this->assertIsString($data);
        $this->assertEquals($data, $project_composer_content['laravel/framework']);
    }
}

