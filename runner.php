<?php
require_once './vendor/autoload.php';

use Dendev\About\About;

$about = new About();
$about->run(true, true);
