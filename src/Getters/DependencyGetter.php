<?php
namespace Dendev\About\Getters;

class DependencyGetter extends AGetter
{
    protected array $_composer_values;

    public function __construct(string $full_path)
    {
        parent::__construct($full_path);
        $this->_composer_values = $this->_call_composer_parser();
    }

    public function get_backend(): string
    {
        $use_filament = $this->get_use_filament();
        $use_backpack = $this->get_use_backpack();

        $label = 'NaN';
        if( $use_filament)
            $label = 'Filament';
        else if( $use_backpack )
            $label = 'Backpack';

        return $label;
    }

    public function get_backend_version(): string
    {
        $use_filament = $this->get_use_filament();
        $use_backpack = $this->get_use_backpack();

        return $use_filament ? $this->get_filament_version() : ($use_backpack ? $this->get_backpack_version() : 'NaN');
    }

    public function get_use_laravel(): ?bool
    {
        return ! is_null( $this->get('composer.require.laravel/framework') );
    }

    public function get_laravel_version(): ?string
    {
        return $this->get('composer.require.laravel/framework');
    }

    public function get_use_backpack(): ?bool
    {
        return ! is_null( $this->get('composer.require.backpack/crud') );
    }

    public function get_backpack_version(): ?string
    {
        return $this->get('composer.require.backpack/crud');
    }

    public function get_use_filament(): ?bool
    {
        return ! is_null( $this->get('composer.require.filament/filament') );
    }

    public function get_filament_version(): ?string
    {
        return $this->get('composer.require.filament/filament');
    }

    public function resolve():array
    {
        return [
            "backend" => $this->get_backend(),
            "backend_version" => $this->get_backend_version(),
            "use_laravel" => $this->get_use_laravel(),
            "laravel_version" => $this->get_laravel_version(),
            "use_backpack" => $this->get_use_backpack(),
            "backpack_version" => $this->get_backpack_version(),
            "use_filament" => $this->get_use_filament(),
            "filament_version" => $this->get_filament_version(),
        ];
    }
}
