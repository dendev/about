<?php
namespace Dendev\About\Getters;

class ServerGetter extends AGetter
{
    protected array $_server_values;

    public function __construct(string $full_path)
    {
        parent::__construct($full_path);

        $this->_server_values = $this->_call_server_parser();
    }

    public function get_type(): ?string
    {
        return $this->_get_server_value('type');
    }

    public function get_identity(): ?string
    {
        $type = $this->_get_server_value('type');
        $subtype = $this->_get_server_value('subtype');
        $ip = $this->_get_server_value('ip');
        $tmp = explode('.', $ip);
        $ip_last = array_pop($tmp);

        $type = $subtype ? $type . '_' . $subtype : $type;

        return $type . '_' . $ip_last;
    }

    public function get_name(): ?string
    {
        $identity = $this->get_identity();

        return ucfirst($identity);
    }

    public function get_os(): ?string
    {
        return $this->_get_server_value('os');
    }

    public function get_ip(): ?string
    {
        return $this->_get_server_value('ip');
    }

    public function resolve():array
    {
        return [
            'identity' => $this->get_identity(),
            'name' => $this->get_name(),
            'type' => $this->get_type(),
            'os' => $this->get_os(),
            'ip' => $this->get_ip()
        ];
    }

    private function _get_server_value(string $key): mixed
    {
        return array_key_exists($key, $this->_server_values) ? $this->_server_values[$key] : false;
    }
}
