<?php

namespace Dendev\About\Getters;

interface IGetter
{
    public function resolve():array;
}
