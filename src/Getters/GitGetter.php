<?php
namespace Dendev\About\Getters;

class GitGetter extends AGetter
{
    public function __construct(string $full_path)
    {
        parent::__construct($full_path);
    }

    public function get_use_git(): ?bool
    {
        return $this->_check_file_exist('.git/config');
    }

    public function get_current_branch(): ?string
    {
        $output = shell_exec("./scripts/git_current_branch.sh {$this->_full_path}" );
        return str_replace(PHP_EOL, '', $output);
    }

    public function get_current_commit(): ?string
    {
        $output = shell_exec("./scripts/git_last_commit.sh {$this->_full_path}");
        return str_replace(PHP_EOL, '', $output);
    }

    public function get_remote_repository(): ?string
    {
        $output = shell_exec("./scripts/git_remote_repository.sh {$this->_full_path}");
        return str_replace(PHP_EOL, '', $output);
    }

    public function get_remote_repository_url(): ?string
    {
        $url = null;

        $output = shell_exec("./scripts/git_remote_repository.sh {$this->_full_path}");
        $output = str_replace(PHP_EOL, '', $output);

        if( str_contains($output, 'gitlab') )
        {
            $tmp = str_replace('git@gitlab.com:', '', $output);
            $tmp = str_replace('.git', '', $tmp);

            $tmp = explode('/', $tmp);
            $vendor = $tmp[0];
            $project_name = $tmp[1];

            $url = "https://gitlab.com/$vendor/$project_name";
        }

        return $url;
    }

    public function get_remote_current_branch(): ?string
    {
         $output = shell_exec("./scripts/git_remote_current_branch.sh {$this->_full_path}");
        return str_replace(PHP_EOL, '', $output);
    }

    public function get_remote_current_commit(): ?string
    {
        $output = shell_exec("./scripts/git_remote_current_commit.sh {$this->_full_path}");
        return str_replace(PHP_EOL, '', $output);
    }

    public function resolve():array
    {
        $current_branch = false;
        $current_commit = false;
        $remote_repository = false;
        $remote_repository_url = false;
        $remote_current_commit = false;
        $remote_current_branch = false;
        $is_sync = false;


        $use_git = $this->get_use_git();
        if( $use_git)
        {
            $current_branch = $this->get_current_branch();
            $current_commit = $this->get_current_commit();

            $remote_repository = $this->get_remote_repository();
            if( $remote_repository)
            {
                /* FIXME remote need auth
                $remote_current_branch = $this->get_remote_current_branch(); // broke with mydocs
                $remote_current_commit = $this->get_remote_current_commit();
                $remote_repository_url = $this->get_remote_repository_url();

                if(( $current_branch === $remote_current_branch) && ( $current_commit === $remote_current_commit ))
                    $is_sync = true;
                */
            }
        }
        return [
            'use_git' => $use_git,
            'is_sync' => $is_sync,
            'current_branch' => $current_branch,
            'current_commit' => $current_commit,
            'remote_repository' => $remote_repository,
            'remote_repository_url' => $remote_repository_url,
            'remote_current_branch' => $remote_current_branch,
            'remote_current_commit' => $remote_current_commit,
        ];
    }
}
