<?php
namespace Dendev\About\Getters;

class AuthenticationGetter extends AGetter
{
    protected  array $_env_values;

    public function __construct(string $full_path)
    {
        parent::__construct($full_path);
        $this->_env_values = $this->_call_env_parser();
    }

    public function get_use_cas(): ?bool
    {
        $enable_cas = $this->get('env.enable_cas');
        $enable_saml = $this->get('env.enable_saml');
        $hostname_cas = $this->get('env.cas_hostname');
        if( is_null($enable_cas) )
        {
            if( ! is_null($hostname_cas) && $enable_saml === 'false')
                return true;
        }
        else
        {
            if( $enable_cas === 'true' && $enable_saml === 'false')
                return true;

        }

        return false;
    }

    public function get_use_saml(): ?bool
    {
        $enable_saml = $this->get('env.enable_saml');
        $enable_cas = $this->get('env.enable_cas');
        $idp_saml = $this->get('env.saml2_idp_entityid');

        if( is_null($enable_saml) )
        {
            if( ! is_null($idp_saml) && $enable_cas === 'false' )
                return true;
        }
        else
        {
            if( $enable_saml === 'true' )
                return true;

        }

        return false;
    }

    public function resolve():array
    {
        $use_saml = $this->get_use_saml();
        $use_cas = $this->get_use_cas();
        $wso2 = $use_saml ? 'Saml' : ( $use_cas ? 'CAS' : 'Nan' );

        return [
            'use_cas' => $use_cas,
            'use_saml' => $use_saml,
            'wso2' => $wso2,
        ];
    }
}
