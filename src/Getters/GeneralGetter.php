<?php
namespace Dendev\About\Getters;


class GeneralGetter extends AGetter
{
    protected array $_env_values;
    protected array $_composer_values;


    public function __construct(string $full_path)
    {
        parent::__construct($full_path);
        $this->_composer_values = $this->_call_composer_parser();
        $this->_env_values = $this->_call_env_parser();
    }

    public function get_app_name(): ?string
    {
        $name = $this->get('env.app_name');
        return is_null($name) ? uniqid() : $name;
    }

    public function get_app_identity(): ?string
    {
        $name = $this->get_app_name();
        $name = str_replace(' ', '-', $name);
        $name = strtolower($name);

        return $name;
    }

    public function get_app_type(): ?string
    {
         return is_null( $this->get('env.app_name') ) ? 'other' : 'laravel';
    }

    public function get_language(): ?string
    {
        return $this->get('composer.require.php') ? 'php' : 'NaN';
    }

    public function get_php_required(): ?string
    {
        return $this->get('composer.require.php');
    }

    public function resolve():array
    {
        $values = [
            'app_name' => $this->get_app_name(),
            'app_identity' => $this->get_app_identity(),
            'app_type' => $this->get_app_type(),
            'language' => $this->get_language(),
            'php_required' => $this->get_php_required(),
        ];


        return $values;
    }
}
