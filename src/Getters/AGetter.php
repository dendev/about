<?php

namespace Dendev\About\Getters;

use Dendev\About\Parsers\ComposerParser;
use Dendev\About\Parsers\EnvParser;
use Dendev\About\Parsers\LaravelModelParser;
use Dendev\About\Parsers\PackageParser;
use Dendev\About\Parsers\ServerParser;

abstract class AGetter implements IGetter
{
    protected string $_full_path;

    protected array $_env_values = [];
    protected array $_composer_values = [];
    protected array $_package_values = [];
    protected array $_server_values = [];


    public function __construct(string $full_path)
    {
        $this->_full_path = $full_path;
    }

    public function get(string $key): ?string
    {
        $value = null;

        if( $this->_check_key_integrity($key))
        {
            $type = $this->_get_key_type($key);
            $field = $this->_get_key_field($key);

            if( $type === 'env' )
            {
                $value = array_key_exists($field, $this->_env_values) ? $this->_env_values[$field] : null;
            }
            else
            {
                $attribute_with_values = '_' . $type . '_values';
                $item_values = $this->$attribute_with_values;

                $levels = explode('.', $field);
                foreach( $levels as $level)
                {
                    if( array_key_exists($level, $item_values))
                    {
                        if( is_array($item_values[$level]) )
                            $item_values = $item_values[$level];
                        else
                            $value = $item_values[$level];
                    }
                }
            }
        }
        else
        {
            echo "ERROR key integrity $key";
        }

        return $value;
    }

    // #
    protected function _call_env_parser(?string $full_path = null) : array
    {
        return $this->_call_parser('env', $full_path);
    }

    protected function _call_composer_parser() : array
    {
        return $this->_call_parser('composer');
    }

    protected function _call_package_parser() : array
    {
        return $this->_call_parser('package');
    }

    protected function _call_server_parser() : array
    {
        return $this->_call_parser('server', $this->_full_path . '.env');
    }

    protected function _call_db_parser() : array
    {
        return $this->_call_parser('db', $this->_full_path);
    }

    protected function _call_laravel_model_parser() : array
    {
        return $this->_call_parser('laravel_model', $this->_full_path);
    }

    protected function _check_key_integrity(string $key): bool
    {
        $available_types = ['env', 'composer', 'package'];

        $is_valid = false;

        if( str_contains($key, '.') )
        {
            $tmp = explode('.', $key);

            if(count($tmp) >= 2)
            {
                if( in_array($tmp[0], $available_types) )
                {
                    $is_valid = true;
                }
            }
        }

        return $is_valid;
    }

    protected function _get_key_type(string $key): string
    {
        $tmp = explode('.', $key);
        return $tmp[0];
    }

    protected function _get_key_field(string $key): string
    {
        $tmp = explode('.', $key);
        unset($tmp[0]);
        return implode('.', $tmp);
    }

    protected function _check_file_exist(string $file_name): bool
    {
        return file_exists($this->_full_path . $file_name);
    }

    // -
    private function _call_parser(string $type, ?string $full_path = null): array
    {
        $available_parsers = [
            'env' => [ 'class' => EnvParser::class, 'file_name' => '.env'],
            'db' => [ 'class' => EnvParser::class, 'file_name' => '.env'], // TODO own parser or use env !? better own ( more generic )
            'composer' => [ 'class' => ComposerParser::class, 'file_name' => 'composer.json'],
            'package' => [ 'class' => PackageParser::class, 'file_name' => 'package.json' ],
            'laravel_model' => [ 'class' => LaravelModelParser::class, 'file_name' => '' ],
            'server' => [ 'class' => ServerParser::class, 'file_name' => '' ],
        ];

        $config = array_key_exists($type, $available_parsers) ? $available_parsers[$type] : false;

        if( $config)
        {
            $parser_class = $config['class'];
            $file_name = $config['file_name'];

            $full_path = $full_path ? $full_path : $this->_full_path;

            $parser = new $parser_class($full_path . $file_name);
            return $parser->resolve();
        }

        return [];
    }
}
