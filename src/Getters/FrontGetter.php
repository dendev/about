<?php
namespace Dendev\About\Getters;

class FrontGetter extends AGetter
{
    protected array $_composer_values;
    protected array $_package_values;


    public function __construct(string $full_path)
    {
        parent::__construct($full_path);
        $this->_composer_values = $this->_call_composer_parser();
        $this->_package_values = $this->_call_package_parser();
    }

    public function get_use_livewire(): ?bool
    {
        return ! is_null($this->get('composer.require.livewire/livewire'));
    }

    public function get_use_codyhouse(): ?bool
    {
        return $this->_check_file_exist('public/css/codystyle.css');
    }

    public function get_use_bootstrap(): ?bool
    {
        if( $this->get('package.dependencies.bootstrap-css-only') || $this->get('package.dependencies.bootstrap') || $this->get('package.devDependencies.bootstrap'))
            return true;

        return false;
    }

    public function get_use_mdbootstrap(): ?bool
    {
        $exist_in_base = $this->_check_file_exist('public/vendor/mdb/base/css/mdb.css');
        $exist_mdb = $this->_check_file_exist('public/vendor/mdb/css/mdb.css');

        return ( $exist_in_base || $exist_mdb);
    }

    public function get_use_tailwindcss(): ?bool
    {
        return ! is_null( $this->get('package.devDependencies.tailwindcss') );
    }

    public function get_use_vite(): ?bool
    {
        return ! is_null( $this->get('package.devDependencies.vite') );
    }

    public function get_use_webpack(): ?bool
    {
       return ! is_null( $this->get('package.devDependencies.laravel-mix') );
    }

    public function get_use_sass(): ?bool
    {
        return ! is_null( $this->get('package.devDependencies.sass') );
    }

    public function get_use_postcss(): ?bool
    {
        return ! is_null($this->get('package.devDependencies.postcss'));
    }

    public function resolve():array
    {
        return [
            'use_livewire' => $this->get_use_livewire(),
            'use_codyhouse' => $this->get_use_codyhouse(),
            'use_bootstrap' => $this->get_use_bootstrap(),
            'use_mdbootstrap' => $this->get_use_mdbootstrap(),
            'use_tailwindcss' => $this->get_use_tailwindcss(),
            'use_vite' => $this->get_use_vite(),
            'use_webpack' => $this->get_use_webpack(),
            'use_sass' => $this->get_use_sass(),
            'use_postcss' => $this->get_use_postcss(),
        ];
    }
}
