<?php

namespace Dendev\About\Getters;

use ParentIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class LaravelModelsGetter extends AGetter
{
    protected array $_db_values;

    public function __construct(string $full_path)
    {
        parent::__construct($full_path);
    }

    public function get_db_identity(): false|string
    {
        return $this->_db_values['db_identity'];
    }

    public function get_db_type(): false|string
    {
        return $this->_db_values['db_type'];
    }

    public function get_server_identity(): false|string
    {
        return $this->_db_values['server_identity'];
    }

    public function get_server_ip(): false|string
    {
        return $this->_db_values['server_ip'];
    }

     public function get_server_username(): false|string
    {
        return $this->_db_values['server_username'];
    }


    public function get_table_identity(): false|string
    {
        return $this->_db_values['table_identity'];
    }

    public function get_table_name(): false|string
    {
        return $this->_db_values['table_name'];
    }

    public function resolve(): array
    {
        $values = [];

        $models_path = $this->_reference_models();

        foreach( $models_path as $model_path)
        {
            $model_getter = new LaravelModelGetter($model_path);
            $model_values = $model_getter->resolve();

            $table_name = $model_values['table_name'];
            $values[$table_name] = $model_values;
        }

        return $values;
    }

    // -
    private function _reference_models(): array
    {
        $paths = [];

        if( $this->_check_file_exist('app/Models'))
        {
            $full_path = str_contains($this->_full_path,'app/Models') ? $this->_full_path : $this->_full_path . 'app/Models/';

            $files = scandir($full_path);

            unset($files[array_search('.', $files, true)]);
            unset($files[array_search('..', $files, true)]);

            foreach( $files as $file )
            {
                $full_path_file = $full_path . $file;
                if( is_file($full_path . $file))
                    $paths[] = $full_path_file;
                else if( is_dir($full_path_file)) // recursive TODO
                {
                    $sub_files = scandir($full_path_file);

                    unset($sub_files[array_search('.', $sub_files, true)]);
                    unset($sub_files[array_search('..', $sub_files, true)]);
                    foreach($sub_files as $sub_file )
                    {
                        $full_path_file_sub = $full_path_file . $sub_file;
                        if( is_file($full_path_file_sub))
                            $paths[] = $full_path_file_sub;
                    }
                }
            }
        }

        return $paths;
    }
}
