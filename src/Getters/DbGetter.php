<?php
namespace Dendev\About\Getters;

class DbGetter extends AGetter
{
    protected array $_db_values;

    public function __construct(string $full_path)
    {
        parent::__construct($full_path);

        $this->_db_values = $this->_call_db_parser();
    }

    public function get_identity(): ?string
    {
        $type = $this->get_type();
        $ip = $this->get_ip();
        $tmp = explode('.', $ip);
        $ip_last = array_pop($tmp);

        return $type . '_' . $ip_last;
    }

    public function get_label(): ?string
    {
        $identity = $this->get_identity();

        return ucfirst(str_replace('_', ' ', $identity) );
    }

    public function get_type(): ?string
    {
        $db_port = $this->_get_db_value('db_port');

        $type = 'NaN';
        if( $db_port == '3306')
            $type = 'mariadb';
        else if( $db_port == '5432')
            $type = 'pgsql';

        return $type;
    }

    public function get_ip(): ?string
    {
        $db_host =  $this->_get_db_value('db_host');
        $ip = gethostbyname($db_host);
        return $ip;
    }

    public function get_server_version(): ?string
    {
        return $this->_get_db_value('ip');
    }

    public function get_client_version(): ?string
    {
        return $this->_get_db_value('ip');
    }

    public function resolve():array
    {
        return [
            'identity' => $this->get_identity(),
            'label' => $this->get_label(),
            'type' => $this->get_type(),
            'ip' => $this->get_ip(),
            //'server_version' => $this->get_server_version(),
            //'client_version' => $this->get_client_version(),
        ];
    }

    private function _get_db_value(string $key): mixed
    {
        return array_key_exists($key, $this->_db_values) ? $this->_db_values[$key] : false;
    }
}
