<?php

namespace Dendev\About\Getters;

class LaravelModelGetter extends AGetter
{
    protected array $_db_values;

    public function __construct(string $full_path)
    {
        parent::__construct($full_path);

        $this->_db_values = $this->_call_laravel_model_parser();
    }

    public function get_db_identity(): false|string
    {
        return $this->_get_db_value('db_identity');
    }

    public function get_db_type(): false|string
    {
        return $this->_get_db_value('db_type');
    }

    public function get_server_identity(): false|string
    {
        return $this->_get_db_value('server_identity');
    }

    public function get_server_ip(): false|string
    {
        return $this->_get_db_value('server_ip');
    }

     public function get_server_username(): false|string
    {
        return $this->_get_db_value('server_username');
    }

    public function get_table_identity(): false|string
    {
        return $this->_get_db_value('table_identity');
    }

    public function get_table_name(): false|string
    {
        return $this->_get_db_value('table_name');
    }

    public function resolve(): array
    {
        return [
            'db_identity' => $this->get_db_identity(),
            'db_type' => $this->get_db_type(),
            'server_identity' => $this->get_server_identity(),
            'server_ip' => $this->get_server_ip(),
            'server_username' => $this->get_server_username(),
            'table_identity' => $this->get_table_identity(),
            'table_name' => $this->get_table_name(),
        ];
    }

    private function _get_db_value($key): mixed
    {
        return array_key_exists($key, $this->_db_values) ? $this->_db_values[$key] : false;
    }
}
