<?php
namespace Dendev\About;

use Dendev\About\Getters\AuthenticationGetter;
use Dendev\About\Getters\DependencyGetter;
use Dendev\About\Getters\FrontGetter;
use Dendev\About\Getters\GeneralGetter;
use Dendev\About\Getters\GitGetter;
use Dendev\About\Getters\LaravelModelsGetter;
use Dendev\About\Getters\ServerGetter;
use Dendev\About\Getters\DbGetter;
use Dendev\About\Referencers\LaravelReferencer;

class About
{
    public function __construct()
    {
        $this->_config = require_once './config.php';
    }

    public function run(bool $send_it = false, bool $save_it = false)
    {
        echo "* Run !\n";
        $summary = [];

        // refs
        echo "** Get projects path\n";
        $laravel_referencer = new LaravelReferencer();
        $laravel_paths = $laravel_referencer->resolve();

        $getters = [
            'general' => GeneralGetter::class,
            'server' => ServerGetter::class,
            'db' => DbGetter::class,
            'authentification' => AuthenticationGetter::class,
            'dependencies' => DependencyGetter::class,
            'front' => FrontGetter::class,
            'git' => GitGetter::class,
            'tables' => LaravelModelsGetter::class
        ];

        echo "** Get projects infos\n";
        foreach ( $laravel_paths as $project_name => $laravel_path )
        {
            echo "\n*** Work on $project_name ( $laravel_path) \n";
            foreach( $getters as $identity => $classname)
            {
                echo "**** run getter $identity\n";
                $getter = new $classname($laravel_path);
                $values = $getter->resolve();

                $summary[$project_name][$identity] = $values;
            }
        }

        if( $send_it )
            $this->_send_it($summary);

        if( $save_it )
            $this->_save_it($summary, 'laravel_summary');


        return $summary;
    }

    private function _save_it($data)
    {
        $json_content = json_encode($data, JSON_PRETTY_PRINT);
        $path = './summaries/laravel_summary.json';
        $ok = file_put_contents($path, $json_content);

        if( $ok )
            echo "\n** Save to $path: success";
        else
            echo "\n** Save to $path: error";

        echo "\n* END\n";

        return $ok;
    }

    private function _send_it($data)
    {
        $is_send = false;

        $json_content = json_encode($data, JSON_PRETTY_PRINT);

        $url = $this->_config['send_to_api_url'];
        $token = $this->_config['send_to_api_token'];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json_content,
            CURLOPT_HTTPHEADER => [
                "Authorization: $token",
                "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            echo "\n** Send to $url: error #:" . $err;
        }
        else
        {
            echo "\n** Send to $url: success";
            $is_send = true;
        }

        return $is_send;
    }
}
