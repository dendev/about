<?php
namespace Dendev\About\Referencers;

interface IReferencer
{
    public function resolve(array $args = null): array;

}
