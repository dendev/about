<?php
namespace Dendev\About\Referencers;

class LaravelReferencer implements IReferencer
{
    private array $_references;

    public function __construct()
    {
        $this->_references = [];
    }

    public function resolve(?array $args = null): array
    {
        $formated = [];

        // use sh find
        $output = shell_exec("./scripts/find_laravel.sh");

        // format
        $files = explode(PHP_EOL, $output);

        foreach( $files as $file )
        {
            if( $file != '' )
            {
                $tmp = explode('/', $file);
                $nb = count($tmp);
                $key = $tmp[$nb - 2];

                $formated[$key] = str_replace('.env', '', $file);
            }
        }

        $this->_references = $formated;

        return $formated;
    }

}
