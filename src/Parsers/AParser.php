<?php

namespace Dendev\About\Parsers;

abstract class AParser implements IParser
{
    protected string $_full_path;

    public function __construct($full_path)
    {
        $this->_full_path = $full_path;
    }

    public function resolve(): array
    {
        if( $this->_check_file() )
            return $this->_read_file();
        return [];
    }

    // -
    private function _check_file(): bool
    {
        $is_valid = false;
        if( file_exists($this->_full_path) && is_readable($this->_full_path ) && is_file($this->_full_path) )
            $is_valid = true;
        else
            echo "ERROR: Unable to access or read file " . $this->_full_path;

        return $is_valid;
    }

    private function _read_file(): array
    {
        $content = [];

        if( $this->_type === 'env' )
            $content = $this->_read_env_file();
        else if( $this->_type === 'json')
            $content = $this->_read_json_file();
        else if( $this->_type === 'laravel_model')
            $content = $this->_read_laravel_model_file();
        else if( $this->_type === 'server')
            $content = $this->_read_server_file();

        return $content;
    }

    private function _read_env_file(): array
    {
        $forbiddens = [
            'app_key', 'saml2_idp_x509', 'mix_pusher_app_key',
            'mail_username',
            'aws_secret_access_key',
            'api_facturation_token',
            'henallux_client_id',
            'henallux_client_secret'
        ];
        $content = [];

        $raw_content = file($this->_full_path);
        foreach( $raw_content as $row )
        {
            if( ! empty($row) )
            {
                if( ! str_contains( $row, 'PASSWORD') )
                {
                    $exploded_row = explode('=', $row);

                    $key = $exploded_row[0];
                    $value = array_key_exists(1, $exploded_row) ? $exploded_row[1] : false;

                    $key = strtolower($key);
                    $value = str_replace(PHP_EOL, '', $value);

                    if( ! in_array($key, $forbiddens) && $key !== PHP_EOL)
                        $content[strtolower($key)] = $value;
                }
            }
        }

        return $content;
    }

    private function _read_json_file(): array
    {
        $forbiddens = [];
        $content = [];

        $raw_content = file_get_contents($this->_full_path);
        $content = json_decode($raw_content, true);

        return $content;
    }

    private function _read_laravel_model_file(): array
    {
        return $this->_get_laravel_db_values();
    }

    private function _read_server_file(): array
    {
        $forbiddens = [];
        $content = [];

        $output = shell_exec("./scripts/informations_server.sh");
        $rows = explode(PHP_EOL, $output);

        foreach ($rows as $row)
        {
            $tmp = explode('=', $row);
            if( count($tmp) === 2 )
            {
                $key = $tmp[0];
                $value = $tmp[1];
                $content[$key] = $value;
            }
        }

        return $content;
    }

    private function _read_db_file(): array
    {
        $forbiddens = [];
        $content = [];

        $output = shell_exec("./scripts/informations_server.sh");
        $rows = explode(PHP_EOL, $output);

        foreach ($rows as $row)
        {
            $tmp = explode('=', $row);
            if( count($tmp) === 2 )
            {
                $key = $tmp[0];
                $value = $tmp[1];
                $content[$key] = $value;
            }
        }

        return $content;
    }

    private function _get_laravel_model_attribute_value(string $attribute): false|string // TODO parser or not !?
    {
        $value = false;

        $lines = file($this->_full_path);
        $count = 0;
        foreach($lines as $line)
        {
            if(str_contains( $line, 'protected') && str_contains($line, $attribute))
            {
                $tmp = explode ( '=', $line);
                if( count($tmp) == 2)
                {
                    $value = $tmp[1];
                    $value = trim($value);
                    $value = str_replace("'", '', $value);
                    $value = str_replace(";", '', $value);
                }
            }
        }

        return $value;
    }

    private function _get_laravel_db_values(): array
    {
        $default_connection_ref = 'db_connection';
        $values = [];

        // get env values
        $tmp = explode('app/Models', $this->_full_path);
        $model_full_path = $this->_full_path;
        $env_full_path = $tmp[0];

        $this->_full_path = $env_full_path . '.env';
        $env_values = $this->_read_env_file();
        $this->_full_path = $model_full_path;


        // get connection
        $connection = $this->_get_laravel_model_attribute_value('$connection');

        $connection = (! $connection ) ? $default_connection_ref : $connection;
        $is_default = $connection === $default_connection_ref;

        // define keys
        $key_values = ['db_connection', 'db_host', 'db_database', 'db_username', 'db_schema', 'db_username'];

        // get value of keys
        foreach( $key_values as $key )
        {
            $values[$key] = array_key_exists($key, $env_values) ? $env_values[$key] : false;
        }

        // define virtual keys
        $values['server_ip'] = array_key_exists('db_host', $values) ? $values['db_host'] : false;
        $values['server_username'] = array_key_exists('db_username', $values) ? $values['db_username'] : false;
        $values['db_type'] = array_key_exists('db_schema', $values) && $values['db_schema'] ? 'pgsql' : 'mariadb';

        if( array_key_exists('db_host', $values))
        {
            // define short ip
            $db_host = $values['db_host'] === 'localhost' ? '127.0.0.1' : $values['db_host'];
            $tmp = explode('.', $db_host);
            $short_ip = array_pop($tmp);

            if ( array_key_exists('db_database', $values) )
                $values['db_identity'] = $values['db_database'] . '_' . $short_ip;

            if ( array_key_exists('db_type', $values))
                $values['server_identity'] = 'db_' . $values['db_type'] . '_' . $short_ip;
        }

        // table
        $table =  $this->_get_laravel_model_attribute_value('$table');

        if( ! $table )
        {
            $tmp = explode('/', $this->_full_path);
            $table = array_pop($tmp);

            $table = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $table));
            $table = str_replace('.php', 's', $table);
            $table = strtolower($table);
        }

        $values['table_identity'] = $values['db_identity'] . '-' . $table;
        $values['table_name'] = $table;

        // end
        return $values;
    }
}
