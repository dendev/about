<?php

namespace Dendev\About\Parsers;

class LaravelModelParser extends AParser
{
    public function __construct(string $full_path)
    {
        parent::__construct($full_path);

        $this->_type = 'laravel_model';
    }
}
