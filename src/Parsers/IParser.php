<?php

namespace Dendev\About\Parsers;

interface IParser
{
    public function resolve(): array;

}
