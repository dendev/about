<?php

namespace Dendev\About\Parsers;

class ComposerParser extends AParser
{
    protected string $_type;

    public function __construct($full_path)
    {
        parent::__construct($full_path);

        $this->_type = 'json';
    }
}
