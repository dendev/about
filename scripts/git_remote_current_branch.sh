#!/bin/bash

## default values
ROOT_PATH='/var/www/html/about';

## values from args cmd_name root_path find_me
if [ ! -z "$1" ]
then
  ROOT_PATH=$1;
fi

## run cmd
cd $ROOT_PATH && git remote show origin | head -n 4 | tail -n 1  | cut -d':' -f 2 | tr -d ' '

exit 0;
