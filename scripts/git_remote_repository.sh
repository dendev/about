#!/bin/bash

## default values
ROOT_PATH='/var/www/html/about';

## values from args cmd_name root_path find_me
if [ ! -z "$1" ]
then
  ROOT_PATH=$1;
fi

## run cmd
cd $ROOT_PATH && git remote -v | head -n 1 | cut -d' ' -f 1 | sed 's/origin//' | tr -d '\t'

exit 0;
