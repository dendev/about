#!/bin/bash

## default values
ROOT_PATH='/var/www/html';
FIND_ME='composer.json';

## values from args cmd_name root_path find_me
if [ ! -z "$1" ]
then
  ROOT_PATH=$1;
fi

if [ ! -z "$2" ]
then
  FIND_ME=$2;
fi

## run cmd
results=();
find $ROOT_PATH -name $FIND_ME |
while read result
do
  echo $result
done
