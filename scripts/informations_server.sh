#!/bin/bash

## run
# will display: ip, os, os_version, is_db, is_web values

### label
hostname=$(hostname);
echo "label=$hostname"

### ip
ok=127;

interface=$(ip route | awk '/default/ { print $5 }' | head -n 1);
ip=$(ip addr show $interface | grep inet | awk '{print $2}' | sed 's/addr://' | head -n 1 | cut -d'/' -f1);

if [ $? -eq 0 ]
then
  echo "ip=$ip"
fi

### os
if [ -x "$(command -v lsb_release )" ]; then
  # ubuntu -> name, version
  os_name=$(cat  /etc/lsb-release | head -n 1 | cut -d'=' -f 2)
  os_version=$(cat  /etc/lsb-release | head -n 2 | tail -n 1 | cut -d'=' -f 2)
  echo "os=$os_name $os_version"
else
  # centos -> name, version
  os_name=$(cat  /etc/os-release | head -n 3 | tail -n 1 | cut -d'=' -f 2)
  os_version=$(cat  /etc/os-release | head -n 3 | tail -n 2 | head -n 1 | cut -d'=' -f 2)
  echo "os=$os_name $os_version"
fi

### type
has_db_mariadb=$(whereis mariadb | cut -d':' -f 2)
has_db_postgresql=$(whereis mariadb | cut -d':' -f 2)
! [[ -z $has_db_mariadb ]] && echo "type=db" && echo "subtype=mariadb"
! [[ -z $has_db_postgresql ]] && echo "type=db" && echo "subtype=pgsql"

has_web=$(whereis apache2 | cut -d':' -f 2)
! [[ -z $has_web ]] && echo "type=web"


exit $ok;
