# About

> Collect application information

## Infos

Get many types from many files types
### General

* type of application
* php required
* 
**Output**
```json
{
  "general": [
    {
      "app_name": "my_app",
      "app_type": "laravel|zend",
      "php_required": "^7.3|^8.0"
    }
  ]
}
```

### Databases

Databases used by applications
* host
* user
* name

**Output**
```json
{
  "databases": [
    {
      "name": "my_db_name",
      "host": "http://my-db-name.local",
      "usr": "allowed_user_name",
      "tables" : [
        {
          "name": "implantations"
        }
      ]
    }
  ]
}
```

#### Tables
Tables used by applications
* database
* name

**Output**
```json
{
  "tables": [
    {
      "name": "my_db_name",
      "host": "http://my-db-name.local",
      "usr": "allowed_user_name"
    }
  ]
}
```


#### Fields
Fields in table used by applications
* table 
* name

( difficult to identify with certainty )

#### Sql
Raw sql found in code
* sql 
* file path && name
* row number

### Authentification
* cas
* saml

**Output**
```json
{
  "authentication": [
    {
      "use_cas": "true",
      "use_saml": "false"
    }
  ]
}
```

### Git
* use_git
* is_sync
* current_branch
* current_commit
* remote_repository
* remote_repository_url
* remote_current_branch
* remote_current_commit

**Output**
```json
{
  "git": [
    {
      "use_git": "true",
      "is_sync": "false",
      "current_branch": "main",
      "current_commit": "5b5aad997603d8bce12b3775df19afbbb00118e5",
       "remote_repository": "git@gitlab.com:dendev/about.git",
      "remote_repository_url": "https://gitlab.com/dendev/about",
      "remote_current_branch":  "main",
      "remote_current_commit":  "ybtaad997603d8fge12b3775df19afbbb0011456",
    }
  ]
}
```

### Dependencies
* laravel_version
* backpack_version
* filament_version

**Output**
```json
{
  "dependencies": [
    {
      "use_laravel": "true",
      "laravel_version": "false",
      "use_backpack": "true",
      "backpack_version": "false",
      "use_filament": "true",
      "filament_version": "false"
    }
  ]
}
```

### Testing coverage

### Texts

### Front
* livewire
* codyhouse
* mdbootstrap
* tailwind
* vite
* webpack
* sass
* postcss

```json
{
  "front": [
    {
      "use_livewire": "true",
      "use_codyhouse": "false",
      "use_mdbootstrap": "false",
      "use_tailwindcss": "false",
      "use_vite": "false",
      "use_webpack": "true",
      "use_sass": "true",
      "use_postcss": "true"
    }
  ]
}
```

### Server
Fields:

* type
* os
* ip

### Diagnostic ( util ? )
